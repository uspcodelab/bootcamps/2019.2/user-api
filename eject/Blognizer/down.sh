#!/bin/env sh

docker-compose -f Gateway/docker-compose.yaml down
echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1mGateway\e[21m is down!\e[0m
    ||

"

for COMPONENT in `ls *Component/ -d`; do
  docker-compose -f ${COMPONENT}docker-compose.yaml down
  echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1m${COMPONENT}\e[21m is down!\e[0m
    ||

  "
done

docker-compose -f docker-compose.yaml down
echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1mBroker\e[21m is down!\e[0m
    ||

"
