import Handler from './Handler';

class UserHandler extends Handler {
  constructor(stanContext: any, opts: any, db: any) {
    super(stanContext, opts, db, 'User');
  }

  protected filterData(payload: any): any {
    return {
      _id: payload._id,
      name: payload.name,
      bio: payload.bio,
      avatar: payload.avatar,
      email: payload.email,
      password_hash: payload.password_hash,
      salt: payload.salt,
      username: payload.username,
      posts: payload.posts,
      follows: payload.follows,
    };
  }

  protected listenUpdates(_id: string): void {
    const stan = this.stanContext.conn;

    const topic: string = `user.${_id}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        await this.db[this.entityName]
          .updateOne({ _id }, payload);
      });

    this.stanContext.subs[topic]
      .on('unsubscribed', () => {
        console.log(`Unsubscribed from 'user.${_id}'`);
      });
  }

  protected cancelUpdates(_id: string): void {
    const topic: string = `user.${_id}`;
    this.stanContext.subs[topic].unsubscribe();
  }
}

export default UserHandler;

