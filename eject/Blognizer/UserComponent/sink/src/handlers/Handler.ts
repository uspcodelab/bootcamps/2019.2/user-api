abstract class Handler {
  constructor(
    protected stanContext: any,
    protected opts: any,
    protected db: any,
    protected entityName: string,
  ) { }

  public run() {
    this.listenCreation();
    this.listenDeletion();
  }

  protected abstract listenUpdates(_id: string): void;
  protected abstract cancelUpdates(_id: string): void;
  protected abstract filterData(payload: any): any;

  private listenCreation(): void {
    const stan = this.stanContext.conn;

    const topic: string = `new.${this.entityName.toLowerCase()}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        const entity = await this.db[this.entityName]
          .create(this.filterData(payload));

        this.listenUpdates(entity._id);
      });
  }

  private listenDeletion(): void {
    const stan = this.stanContext.conn;

    const topic: string = `delete.${this.entityName.toLowerCase()}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload: { _id } }: any = JSON
          .parse(msg.getData());

        await this.db[this.entityName]
          .deleteOne({ _id });

        this.cancelUpdates(_id);
      });
  }
}

export default Handler;
