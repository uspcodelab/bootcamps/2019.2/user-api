import fs from 'fs';

import db from 'src/db/models';
import stanContext from 'src/stan';
import { Stan } from 'node-nats-streaming';
import Handler from './handlers/Handler';

const stan: Stan = stanContext.conn;
const handlers: any[] = [];

fs.readdirSync('./src/handlers')
  .filter((fileName: string) => fileName !== ('Handler.ts'))
  .forEach(async (fileName: string) => {
    const name: string = fileName.slice(0, fileName.length - 3);
    const handler: any = await import(`${__dirname}/handlers/${name}`);
    handlers.push(handler.default);
  });

stan.on('connect', () => {
  console.log('Sink running!');

  const replayAll: any = stan
    .subscriptionOptions()
    .setDeliverAllAvailable();

  handlers.forEach((Handler: any) => {
    const h: Handler = new Handler(stanContext, replayAll, db);
    h.run();
  });
});
