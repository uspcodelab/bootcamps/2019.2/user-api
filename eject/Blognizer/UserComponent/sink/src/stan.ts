import { connect, Stan, Subscription } from 'node-nats-streaming';

const brokerUrl: string = process.env.BROKER_URL
  ? process.env.BROKER_URL
  : 'nats://broker:4222';

const brokerCluster: string = process.env.BROKER_CLUSTER
  ? process.env.BROKER_CLUSTER
  : 'test-cluset';

const brokerClientId: string = process.env.BROKER_CLIENT_ID
  ? process.env.BROKER_CLIENT_ID
  : 'api';

interface StanContext {
  conn: Stan;
  subs: {
    [key: string]: Subscription;
  };
}

const stanContext: StanContext = {
  conn: connect(brokerCluster, brokerClientId, {
    url: brokerUrl,
  }),
  subs: {},
}

export default stanContext;
