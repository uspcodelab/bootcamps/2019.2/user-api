import { ApolloError } from "apollo-server-koa";
import jwt from "jsonwebtoken";

import { Md5 } from "ts-md5";

async function login(
    _parent: any,
    args: any,
    context: any,
    _info: any,
): Promise<any> {
    const { data } = args;
    const { username, password } = data;
    const { db: { User } } = context;
    const app_secret = "batata";

    const user = await User.findOne({
        username,
    });

    if (!user) {
        throw new ApolloError("Username not found", "404");
    }

    const hash = Md5.hashStr(password + user.salt);
    if (hash !== user.password_hash) {
        throw new ApolloError("Password incorrect", "401");
    }

    const authPayload = {
        token: jwt.sign(
            {
                user_id: user._id
            }, app_secret
        )
    };

    return authPayload;
}

export default login;