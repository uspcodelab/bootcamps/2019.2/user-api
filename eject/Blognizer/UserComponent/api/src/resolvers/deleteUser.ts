import { ApolloError } from 'apollo-server-koa';

import UserDeletedEvent from '../events/UserDeletedEvent';

async function deleteUser(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { User }, stan }: any = context;
  const {
    _id,
  }: any = args;

  let user = await User.findOne({
    _id,
  });

  if (!user) {
    throw new ApolloError('User does not exist', '409');
  }

  const event = new UserDeletedEvent(user);
  stan.publish(event.topic, event.serialized());

  return user;
}

export default deleteUser;
