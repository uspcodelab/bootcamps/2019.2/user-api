import { ApolloError } from "apollo-server-koa";

async function getUserData(_parent: any, args: any, context: any, _info: any): Promise<any>{
    const { db: { UserData } }: any = context;
    const {
      username,
    }: any = args;

    let posts = await UserData.findOne({
        username,
      });
    
      if (!posts) {
        throw new ApolloError('Username not found','409');
      }
    
      return UserData["posts"];
}

export default getUserData;
