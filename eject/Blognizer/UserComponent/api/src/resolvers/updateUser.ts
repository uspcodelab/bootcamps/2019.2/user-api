import { ApolloError } from 'apollo-server-koa';

import UserUpdatedEvent from 'src/events/UserUpdatedEvent';

async function updateUser(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { User}, stan }: any = context;
  const {
    _id,
    data 
  }: any = args;

  let user = await User.findOne({
    _id,
  });

  if (!user) {
    throw new ApolloError('Cannot find User', '409');
  }

  user = {
    ...user,
    ...data 
  };

  const event = new UserUpdatedEvent(user);
  stan.publish(event.topic, event.serialized());

  return user;
}

export default updateUser;

