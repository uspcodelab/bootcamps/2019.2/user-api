import { ApolloError } from 'apollo-server-koa';

async function getUser(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { User } }: any = context;
  const {
    _id,
  }: any = args;

  let user = await User.findOne({
    _id,
  });

  if (!user) {
    throw new ApolloError('User not found', '409');
  }

  return user;
}

export default getUser;
