import { ApolloError } from "apollo-server-koa";
import jwt from "jsonwebtoken";
import UserFollowEvent from "../events/UserFollowEvent";
import stan from "src/stan";


async function follow(
    _parent: any,
    args: any,
    context: any,
    _info: any,
): Promise<any> {
    const { data } = args;
    const { token, followed_username } = data;
    const { db: { User } } = context;
    const app_secret = "batata";
    const token_data = await jwt.verify(token, app_secret);
    console.log(token_data);
    const user_id = (token_data as any).user_id;
    console.log(followed_username);
    console.log(user_id);

    const followed = await User.findOne({
        username: followed_username,
    });
    console.log(followed);
    const follower = await User.findOne({
        _id: user_id,
    });

    if (!followed) {
        throw new ApolloError("User not found", "404");
    }
    if (!follower) {
        throw new ApolloError("Invalid user", "403");
    }
    const follow_payload = {
        followed_id: followed._id,
        follower_id: follower._id
    }

    const event = new UserFollowEvent(follow_payload);
    stan.publish(event.topic, event.serialized());
    return follow_payload;
}

export default follow;