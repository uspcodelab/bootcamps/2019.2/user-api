import { ApolloError } from "apollo-server-koa";

import UserCreatedEvent from "../events/UserCreatedEvent";
import { Md5 } from "ts-md5";

async function createUser(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const {
    db: { User },
    stan,
    uuidv4,
  }: any = context;
  const { data }: any = args;
  const { username, email, password, repeat_password } = data;

  if (password !== repeat_password) {
    throw new ApolloError("Passwords don't match", "400");
  }

  let user = await User.findOne({
    username,
  });

  if (user) {
    throw new ApolloError("Username already taken", "409");
  }

  user = await User.findOne({
    email,
  });

  if (user) {
    throw new ApolloError("Email already registered", "409");
  }

  const salt = Math.random()
    .toString(36)
    .substring(2);

  const password_hash = Md5.hashStr(password + salt);

  delete data["password"];
  delete data["repeat_password"];

  user = {
    _id: uuidv4(),
    ...data,
    salt,
    password_hash,
  };

  const event = new UserCreatedEvent(user);
  stan.publish(event.topic, event.serialized());
  return user;
}

export default createUser;
