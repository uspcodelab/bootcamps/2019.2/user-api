import { Document, model, Model, Schema } from 'mongoose';
import { Post, PostSchema } from './Post';


export interface User extends Document {
  _id: string;
  name: string;
  bio: string;
  avatar?: string;
  email: string;
  password_hash: string;
  salt: string;
  username: string;
  posts: Post[];
  follows: User[];
}

export declare type TUser = Model<User>;

export const UserSchema: Schema = new Schema({
  _id: String,
  name: String,
  bio: String,
  avatar: String,
  email: String,
  password_hash: String,
  salt: String,
  username: String,
  posts: [PostSchema],
  follows: [this],
}, {
  _id: false,
});

const User: TUser = model<User>('User', UserSchema);

export default User;

