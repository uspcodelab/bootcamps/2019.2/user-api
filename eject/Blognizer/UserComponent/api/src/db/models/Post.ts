import { Document, model, Model, Schema } from 'mongoose';


export interface Post extends Document {
  _id: string;
  title: string;
}

export declare type TPost = Model<Post>;

export const PostSchema: Schema = new Schema({
  _id: String,
  title: String,
}, {
  _id: false,
});

const Post: TPost = model<Post>('Post', PostSchema);

export default Post;

