class Event {
  private eventType: string;
  private eventTopic: string;
  private eventPayload: any;

  constructor(type: string, topic: string, payload: any) {
    this.eventType = type;
    this.eventTopic = topic;
    this.eventPayload = payload;
  }

  get topic() {
    return this.eventTopic;
  }

  get type() {
    return this.eventType;
  }

  public serialized(): string {
    const copy: any = {
      topic: this.eventTopic,
      payload: this.eventPayload,
    };

    return JSON.stringify(copy);
  }
}

export default Event;
