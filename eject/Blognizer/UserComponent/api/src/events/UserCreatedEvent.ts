import Event from './Event';

class UserCreatedEvent extends Event {
  constructor(entity: any) {
    super('NewUserCreated', 'new.user', entity);
  }
}

export default UserCreatedEvent;
