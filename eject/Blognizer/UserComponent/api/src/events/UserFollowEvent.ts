import Event from './Event';

class UserFollowEvent extends Event {
  constructor(entity: any) {
    super('NewUserFollow', 'user.follow', entity);
  }
}

export default UserFollowEvent;