import Event from './Event';

class UserUpdatedEvent extends Event {
  constructor(entity: any) {
    super('UserUpdated', `user.${entity._id}`, entity);
  }
}

export default UserUpdatedEvent;

