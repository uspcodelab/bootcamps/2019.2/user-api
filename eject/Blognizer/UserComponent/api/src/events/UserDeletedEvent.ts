import Event from './Event';

class UserDeletedEvent extends Event {
  constructor(entity: any) {
    super('UserDeleted', 'delete.user', entity);
  }
}

export default UserDeletedEvent;
