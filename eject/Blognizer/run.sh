#!/bin/env sh

docker-compose -f docker-compose.yaml up --build -d
echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1mBroker\e[21m is up!\e[0m
    ||

"

for COMPONENT in `ls *Component/ -d`; do
  docker-compose -f ${COMPONENT}docker-compose.yaml up --build -d
  echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1m${COMPONENT}\e[21m is up!\e[0m
    ||

  "
done

sleep $(expr 5 \* $(ls *Component/ -dl | wc -l))

docker-compose -f Gateway/docker-compose.yaml up --build -d
echo -e "

    ||
    ||  \xE2\x9C\x94 \e[32m\e[1mGateway\e[21m is up!\e[0m
    ||

"
